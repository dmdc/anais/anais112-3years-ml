# ANAIS-112 3 years data - reanalyzed with improved event selection by Machine Learning ( JCAP 11 (2022) 048, JCAP 06 (2023) E01 (erratum) )

# arXiv:2404.17348v2 //Accepted in Communication Physics

## Overview

9 detectors 
all detector has same mass and energy threshold
* MASS of every detector: 12.5 kg
* Energy threshold: 1 keV (*)
* Live time temporal distribution: see files ANAIS112liveTime_3y_10days_D?.csv -> for every detector(0-8)
Live time D0: 1004.68 days
Live time D1: 1011.76 days
Live time D2: 1006.93 days
Live time D3: 1009.11 days
Live time D4: 1008.80 days
Live time D5: 1008.23 days
Live time D6: 1008.88 days
Live time D7: 1010.25 days
Live time D8: 1007.34 days
* Total exposure: 323.05 kg x yr
* Total effective exposure: 312.53 kg x yr (**)
* Energy resolution from 0 to 25 keV
calculated for the summ of all detectors during the first year of data taking:
$`\sigma = (-0.008 \pm 0.001) + (0.378 \pm 0.002)\times \sqrt{\rm{E(keV)}}`$ [Eur. Phys. J. C (2019) 79:228](https://doi.org/10.1140/epjc/s10052-019-6697-4)

notes:
(*) all energies are given in keVee. For simplicity we use keV symbol for keVee
(**) Efective exposure after subtracting periods rejected in the analysis (muon and rate cuts)


**********************
**********************
## EVENT DATA:
--------------------
Experimental bkg in counts/keV/kg/day (corrected by efficiency and live time)

ANAIS112_3y_10days_1.0keV_6.0keV_D?.csv -> bkg vs time for every detector(0-8) integrated in the energy region [1-6] keV in 10-days bins

ANAIS112_3y_10days_2.0keV_6.0keV_D?.csv -> bkg vs time for every detector(0-8) integrated in the energy region [2-6] keV in 10-days bins

ANAIS112_3y_10days_1.3keV_4.0keV_D?.csv -> bkg vs time for every detector(0-8) integrated in the energy region [1.3-4] keV in 10-days bins

Format:
  bin_center(days) , bkg(counts/keV/kg/d) , error(counts/keV/kg/d)

**********************
**********************
## Background Data:
--------------------
MC simulated bkg in counts/kg/day

ANAIS112bkg_M1_1.0keV_6.0keV_D?.csv -> MC simulated background vs time for every detector(0-8) integrated in the energy region [1-6] keV in 15-days bins

ANAIS112bkg_M1_2.0keV_6.0keV_D?.csv -> MC simulated background vs time for every detector(0-8) integrated in the energy region [2-6] keV in 15-days bins

ANAIS112bkg_M1_1.3keV_4.0keV_D?.csv -> MC simulated background vs time for every detector(0-8) integrated in the energy region [1.3-4] keV in 15-days bins

Format:
  bin_center(days) , bkg(counts/kg/d)

**********************
**********************
## Efficiency
--------------------
ANAIS112eff_3y_D?.csv -> efficiency vs energy for every detector(0-8) from 1-6 keV

Format:
  bin_center(keV) , efficiency , error

**********************
**********************
## Live Time
--------------------
Live time in days for every 10-days bin

ANAIS112liveTime_3y_10days_D?.csv -> for every detector(0-8)

Format: 
  live_time(days)


**********************
**********************
## Chi2 minimization according to 2404.17348 equations (6) and (7)

a112modFit.C
fitting root macro (roofit)
ROOT Version: 6.20/00

perform the chi2 minimization according to arXiv:2404.17348v2 equations (6) and (7) 

output: figures (2), (3) and (5)

usage:
a112modFit(double eneI, double eneE, bool useMC=1)

input parameters:

* eneI, eneE: initial and end energies. Possible values: 
1, 6 -> fit [1-6] keV (figure 2)
2, 6 -> fit [2-6] keV (figure 3)
1.3, 4 -> fit [1.3-4] keV (figure 5)

* useMC: background model 
1 (default) -> use MC background model (equation 7)
0 -> use single exponential approximation for background (this result is not included in arXiv:2404.17348v2)

